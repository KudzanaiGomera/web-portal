import { TestBed } from '@angular/core/testing';

import { SharedgalleryService } from './sharedgallery.service';

describe('SharedgalleryService', () => {
  let service: SharedgalleryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SharedgalleryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
