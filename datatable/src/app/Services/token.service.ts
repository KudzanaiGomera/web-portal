import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  url = environment.url;

  private iss = {
    login : `${this.url}/V2/api/login/`,
    selectsupplier : `${this.url}/V2/api/userstoresuppliers/?storeid=SRCH30376&channel=SRCH&_ts=1624054505559`,
    sitedayscover: `${this.url}/qsapi/PNPZEROSTOCK.php?cid=1093`,
    reports: `${this.url}/V2/v2api/SDC.php?sid=1077&channel=5&locationType=STORES&clearcache=0`,
    numericdistribution: `${this.url}/qsapi/PNPZEROSTOCK.php?cid=1085&channel=2`,
  }

  constructor () {}

  handle(token) {
    this.set(token);
  }

  set(token){
    localStorage.setItem('token', token);
  }

  get(){
    return localStorage.getItem('token');
  }

  remove() {
    localStorage.removeItem('token');
  }


  isValid() {
    const token = this.get();
    if (token && 'undefined' != token) {
      if (this.tokenExpired(token)) {
        console.log("Token has expired");
        return
      } else {
        // console.log("Token is valid");
        const payload = this.payload(token);
        if (payload) {
          return Object.values(this.iss).indexOf(payload.iss) >= -1 ? true : false;
        }
      }

    }
    return false;
  }

  payload(token){
    const payload = token.split('.')[1];
    return this.decode(payload);
  }

  decode(payload) {
    // console.log(atob(payload));
    return JSON.parse(atob(payload));
  }

  private tokenExpired(token: string) {
    const expiry = (JSON.parse(atob(token.split('.')[1]))).exp;
    return (Math.floor((new Date).getTime() / 1000)) >= expiry;
  }

  loggedIn() {
    return this.isValid();
  }
}
