interface Scripts {
  name: string;
  src: string;
}
export const ScriptStore: Scripts[] = [
  {name: 'off-canvas', src: './assets/js/off-canvas.js'},
  {name: 'template', src: './assets/js/template.js'},
  {name: 'gallery', src: './assets/js/gallery.js'},
  {name: 'collapse', src: './assets/js/hoverable-collapse.js'},
  {name: 'settings', src: './assets/js/settings.js'},
  {name: 'todolist', src: './assets/js/todolist.js'},
];
