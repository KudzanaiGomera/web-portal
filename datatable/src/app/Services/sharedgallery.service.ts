import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Gallery } from './../components/gallery/galleryObj';

@Injectable({
  providedIn: 'root'
})
export class SharedgalleryService {

  private availablesuppliers = new BehaviorSubject<Gallery[]>(null);

  constructor() { }

  loadGallery(suppliers: Gallery[]){
    this.availablesuppliers.next(suppliers);
  }

  getGallery(): Observable<Gallery[]> {
    return this.availablesuppliers.asObservable();
  }
}