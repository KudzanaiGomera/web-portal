import { TestBed } from '@angular/core/testing';

import { SelectsupplierService } from './selectsupplier.service';

describe('SelectsupplierService', () => {
  let service: SelectsupplierService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SelectsupplierService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
