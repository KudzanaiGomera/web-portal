import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

import { Supplier } from '../components/selectsupplier/selectsupplierObj';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  private availablesuppliers = new BehaviorSubject<Supplier[]>(null);

  constructor() { }

  loadSuppliers(suppliers: Supplier[]){
    this.availablesuppliers.next(suppliers);
  }

  getSuppliers(): Observable<Supplier[]> {
    return this.availablesuppliers.asObservable();
  }
}
