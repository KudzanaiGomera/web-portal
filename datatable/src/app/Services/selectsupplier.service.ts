import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SelectsupplierService {
  url = environment.url;

  constructor(
    private http: HttpClient,
    ) {}

  selectsupplier() {
    return this.http.get(`${this.url}/V2/api/useraccountdetails/?=ts1624975780647`).pipe(

      map(results => {
          console.log("result in selectservice",results);
          return results['DataResult']['LinkedSuppliers'];
        })
    );
  }

}
