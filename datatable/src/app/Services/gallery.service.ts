import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GalleryService {

  url = environment.url;

  constructor(
    private http: HttpClient,
    ) {}

  suppliergallery(id) {
    return this.http.get(`${this.url}/V2/api/photogallery/?supplier=${id}`).pipe(
      map(results => {
          // console.log("result in galleryservice",results);
          return results['DataResult'];
        })
    );
  }
}
