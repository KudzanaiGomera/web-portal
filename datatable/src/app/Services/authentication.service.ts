import { Injectable } from '@angular/core';
import { HttpClient, HttpBackend } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  url = environment.url;

  constructor(
    private http: HttpClient,
    handler: HttpBackend
    ) {
      this.http = new HttpClient(handler);
    }

  login(data) {
    return this.http.post(`${this.url}/V2/api/login/`, data)
  }

  sitedaycover() {
    return this.http.get(`${this.url}/qsapi/PNPZEROSTOCK.php?cid=1093`)
  }

  numericdistribution() {
    return this.http.get(`${this.url}/qsapi/PNPZEROSTOCK.php?cid=1085&channel=2`)
  }

  reports() {
    return this.http.get(`${this.url}/V2/v2api/SDC.php?sid=1077&channel=5&locationType=STORES&clearcache=0`)
  }

}
