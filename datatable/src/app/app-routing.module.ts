import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GalleryComponent } from './components/gallery/gallery.component';
import { LoginComponent } from './components/login/login.component';
import { NumericDistributionComponent } from './components/numeric-distribution/numeric-distribution.component';
import { PageNotFoundComponent } from './Components/page-not-found/page-not-found.component';
import { ReportsComponent } from './components/reports/reports.component';
import { SelectsupplierComponent } from './components/selectsupplier/selectsupplier.component';
import { SiteDaysCoverComponent } from './components/site-days-cover/site-days-cover.component';
import { AfterLoginService } from './Services/after-login.service';
import { BeforeLoginService } from './Services/before-login.service';

const appRoutes:Routes = [

  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },

  {
    path: 'login',
    component: LoginComponent,
    canActivate: [BeforeLoginService]
  },

  {
    path: 'selectsupplier',
    component: SelectsupplierComponent,
    canActivate: [AfterLoginService]
  },

  {
    path: 'sitedayscover/:id',
    component: SiteDaysCoverComponent,
    canActivate: [AfterLoginService]
  },

  {
    path: 'status/:id',
    component: ReportsComponent,
    canActivate: [AfterLoginService]
  },

  {
    path: 'numericdistribution/:id',
    component: NumericDistributionComponent,
    canActivate: [AfterLoginService]
  },

  {
    path: 'gallery/:id',
    component: GalleryComponent,
    canActivate: [AfterLoginService]
  },

  {
    path: '**',
    component: PageNotFoundComponent,
  }

]

@NgModule({
  declarations: [],
  imports: [
RouterModule.forRoot(appRoutes, { useHash: true }),
  ]
})
export class AppRoutingModule { }
