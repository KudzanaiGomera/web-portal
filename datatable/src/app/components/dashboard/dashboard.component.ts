import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/Services/auth.service';
import { TokenService } from 'src/app/Services/token.service';
import { Supplier } from '../selectsupplier/selectsupplierObj';
import { SharedService } from 'src/app/Services/shared.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public loggedIn : boolean;

  public SupplierID;
  public supplier: Supplier[] = [];

  constructor(
    private Auth : AuthService,
    private router: Router,
    private Token: TokenService,
    private shared: SharedService,
    ) { }

    get(){
      return localStorage.getItem('supplierid');
    }

    // get user(): any {
    //   return localStorage.getItem('supplierid');
    // }

  ngOnInit(){
    console.log(this.get());
    this.SupplierID = this.get(),
    console.log(this.SupplierID);
    this.Auth.authStatus.subscribe(value => this.loggedIn = value);
    this.shared.getSuppliers()
    .pipe(
      map(suppliers =>
        // console.log("suppliers", suppliers)
        suppliers.filter(supplier => supplier.SupplierID == this.SupplierID),
      )
    )
    .subscribe(response => {
      console.log("From shared services", response);
      console.log(this.SupplierID);
      this.supplier = response;

    });
  }

  logout(event: MouseEvent) {
    event.preventDefault();
    this.Token.remove();
    localStorage.removeItem('supplierid');
    this.Auth.changeAuthStatus(false);
    this.router.navigateByUrl('/');
  }

}
