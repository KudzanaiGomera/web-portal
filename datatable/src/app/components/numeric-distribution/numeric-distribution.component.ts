import { Component, OnInit, OnDestroy } from '@angular/core';
import { CustomTooltipComponent } from '../custom-tooltip/custom-tooltip.component';
import { AuthService } from 'src/app/Services/auth.service';
import { AuthenticationService } from 'src/app/Services/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SharedService } from 'src/app/Services/shared.service';
import { TokenService } from 'src/app/Services/token.service';
import { Supplier } from '../selectsupplier/selectsupplierObj';
import { filter, map, takeUntil } from 'rxjs/operators';
import { SubSink } from 'subsink';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-numeric-distribution',
  templateUrl: './numeric-distribution.component.html',
  styleUrls: ['./numeric-distribution.component.css']
})
export class NumericDistributionComponent implements OnInit, OnDestroy{

  public loggedIn: boolean;
  public searchValue;
  public gridApi;
  public gridColumnApi;
  public columnDefs;
  public sortingOrder;
  public NoOfStores;
  public tooltipShowDelay;
  public frameworkComponents;
  data: any[] = [];

  public SupplierID;
  public supplier: Supplier[] = [];

  subs = new SubSink();

  constructor(
    private Authentication: AuthenticationService,
    private Auth: AuthService,
    private route: ActivatedRoute,
    private shared: SharedService,
    private router: Router,
    private Token: TokenService,
  ) {

   }

   //Columns
   Numeric(){
    this.columnDefs = [
      {
        headerName: 'Province',
        field: 'Province',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'StoreName',
        field: 'StoreName',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'Profile',
        field: 'Profile',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'Size',
        field: 'Size',
        width: 100,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'ArticleDescription',
        field: 'ArticleDescription',
        width: 300,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'SiteArticleStatus',
        field: 'SiteArticleStatus',
        width: 60,
        sortingOrder: ["asc", "desc"],
        filter:true,
        resizable: true,
        sortable: true,
        tooltipField: 'SiteArticleStatus',
        tooltipComponent: 'customTooltip',
        tooltipComponentParams: { color: '#FFFEE3' },
        valueParser: numberParser,
        cellStyle: cellStyle,
      },
  ];

  this.sortingOrder=["desc", "asc", null];

  this.tooltipShowDelay = 0;

  this.frameworkComponents = { customTooltip: CustomTooltipComponent };

  }

  getSupplierID(){
    let supplierID = this.route.snapshot.paramMap.get('id');
    return (supplierID);
  }

  ngOnInit() {
    //inject:js here
    // temporary bug fix due to js not loading on first page visit now load it from ngOnInit
    this.loadScript('./assets/js/off-canvas.js');
    this.loadScript('./assets/js/template.js');
    this.loadScript('./assets/js/hoverable-collapse.js');
    this.loadScript('./assets/js/settings.js');
    this.loadScript('./assets/js/todolist.js');
    //call function on page load
    this.Numeric();

    this.SupplierID = this.getSupplierID();

    //fnc call
    this.service(this.SupplierID);

    //retrive object and store in supplier
    var retrievedObject = localStorage.getItem('supplierObject');
    this.supplier = JSON.parse(retrievedObject);
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  //get the observable and store in localStorage
  service(id){
    this.subs.add(this.Auth.authStatus.subscribe(value => this.loggedIn = value));
    this.subs.add(this.shared.getSuppliers()
    .pipe(
      filter(emp => emp !== null),
      map(suppliers =>
        // console.log("suppliers", suppliers)
        suppliers.filter(supplier => supplier.SupplierID === id),
      )
    )
    .subscribe(response => {
      // console.log("From shared services", response);
      // console.log(this.SupplierID);
      // this.supplier = response;

      localStorage.setItem('supplierObject', JSON.stringify(response));
    }))
  }

  logout(event: MouseEvent) {
    event.preventDefault();
    this.Token.remove();
    localStorage.removeItem('supplierid');
    localStorage.removeItem('supplierObject');
    this.Auth.changeAuthStatus(false);
    this.router.navigateByUrl('/');
  }

  //pull data from api and populate the grid
   onGridReady(params){
    this.gridApi= params.api;
    this.gridColumnApi = params.columnApi;
    this.Authentication.numericdistribution()
    .toPromise().then(res => {
      this.data = res['STOCKOUT'];
      params.api.setRowData(this.data);
      })
 }

 //function to export reports
   onBtExport(){
     var params = {

     };
     // export to csv
     this.gridApi.exportDataAsCsv(params)
   }

   // function for a filtering all columns at once
   quickSearch() {
     this.gridApi.setQuickFilter(this.searchValue);
   }

   //get number of rows displayed which is the number of stores
   numberofStores() {
    this.NoOfStores = this.gridApi.getDisplayedRowCount();
   }

   // load js file in components
   loadScript(url: string) {
    const body = <HTMLDivElement> document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = url;
    script.async = false;
    script.defer = true;
    body.appendChild(script);
  }

}

// styling each cell depending on the condition
function cellStyle(params) {
  var color = numberToColor(params.value);
  return { backgroundColor: color };
}

// change number to correspond colors

//remove the last ones on 2 fist if and else 0 and 4
function numberToColor(val) {
  if (val === '72' || val === '07' || val === '71') {
    return 'red; color: red';
  } else if (val === '04' || val === '02' || val==='05') {
    return 'green; color: green';
  } else if (val === '61' || val === '62') {
    return '#EABC07';
  }
}

function numberParser(params) {
  var newValue = params.newValue;
  var valueAsNumber;
  if (newValue === null || newValue === undefined || newValue === '') {
    valueAsNumber = null;
  } else {
    valueAsNumber = parseFloat(params.newValue);
  }
  return valueAsNumber;
}
