import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NumericDistributionComponent } from './numeric-distribution.component';

describe('NumericDistributionComponent', () => {
  let component: NumericDistributionComponent;
  let fixture: ComponentFixture<NumericDistributionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NumericDistributionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NumericDistributionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
