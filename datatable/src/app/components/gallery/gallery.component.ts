import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { AuthService } from 'src/app/Services/auth.service';
import { GalleryService } from 'src/app/Services/gallery.service';
import { SharedgalleryService } from 'src/app/Services/sharedgallery.service';
import { TokenService } from 'src/app/Services/token.service';
import { Supplier } from '../selectsupplier/selectsupplierObj';
import { Gallery } from './galleryObj';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {
  public loggedIn: boolean;
  public SupplierID;
  public id;
  data: any[] = [];
  public supplier: Supplier[] = [];
  public gallery: Gallery[] = [];

  constructor(
    private Auth: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private Token: TokenService,
    private Gallery: GalleryService,
    private Sharedgallery: SharedgalleryService,
  ) { }

  getSupplierID(){
    let supplierID = this.route.snapshot.paramMap.get('id');
    return (supplierID);
  }

   ngOnInit() {
     //inject:js here
    // temporary bug fix due to js not loading on first page visit now load it from ngOnInit
    this.loadScript('./assets/js/off-canvas.js');
    this.loadScript('./assets/js/template.js');
    this.loadScript('./assets/js/gallery.js');
    this.loadScript('./assets/js/hoverable-collapse.js');
    this.loadScript('./assets/js/settings.js');
    this.loadScript('./assets/js/todolist.js');
    this.SupplierID = this.getSupplierID();

    //fnc call
    // this.service(this.SupplierID);

    //retrive object and store in supplier
    var retrievedObject = localStorage.getItem('supplierObject');
    this.supplier = JSON.parse(retrievedObject);

    console.log(this.SupplierID);

    this.Auth.authStatus.subscribe(value => this.loggedIn = value);

    this.Gallery.suppliergallery(this.SupplierID)
    .subscribe(
      (data : Gallery[])=>{
        this.gallery = data;
        // this.Sharedgallery.loadGallery(data);
        console.log("this data",this.gallery);
      },
      (error) => console.log(error),
    );
  }

  logout(event: MouseEvent) {
    event.preventDefault();
    this.Token.remove();
    localStorage.removeItem('supplierid');
    localStorage.removeItem('supplierObject');
    this.Auth.changeAuthStatus(false);
    this.router.navigateByUrl('/');
  }

   // load js file in components
   loadScript(url: string) {
    const body = <HTMLDivElement> document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = url;
    script.async = false;
    script.defer = true;
    body.appendChild(script);
  }
}
