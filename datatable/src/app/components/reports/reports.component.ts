import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { filter, map} from 'rxjs/operators';
import { AuthService } from 'src/app/Services/auth.service';
import { AuthenticationService } from 'src/app/Services/authentication.service';
import { SharedService } from 'src/app/Services/shared.service';
import { TokenService } from 'src/app/Services/token.service';
import { Supplier } from '../selectsupplier/selectsupplierObj';
import { SubSink } from 'subsink';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit, OnDestroy{
  public loggedIn: boolean;
  public searchValue;
  public gridApi;
  public gridColumnApi;
  public columnDefs;
  public sortingOrder;


  data: any[] = [];
  public SupplierID;
  public supplier: Supplier[] = [];

  subs = new SubSink();

  constructor(
    private Authentication: AuthenticationService,
    private Auth: AuthService,
    private router: Router,
    private Token: TokenService,
    private route: ActivatedRoute,
    private shared: SharedService,
  ) {

   }

   getSupplierID(){
    let supplierID = this.route.snapshot.paramMap.get('id');
    return (supplierID);
  }

   ngOnInit() {
    //inject:js here
    // temporary bug fix due to js not loading on first page visit now load it from ngOnInit
    this.loadScript('./assets/js/off-canvas.js');
    this.loadScript('./assets/js/template.js');
    this.loadScript('./assets/js/hoverable-collapse.js');
    this.loadScript('./assets/js/settings.js');
    this.loadScript('./assets/js/todolist.js');
    this.columnDefs = [

        {
          headerName: 'SupplierID',
          field: 'supplierID',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'Province',
          field: 'province',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'Banner',
          field: 'banner',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'StoreName',
          field: 'storeName',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'StoreID',
          field: 'storeID',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'ArticleDescription',
          field: 'articleDescription',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'ArticleNumber',
          field: 'articleNumber',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'IsApproximate',
          field: 'isApproximate',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'RateOfSaleChannel',
          field: 'rateOfSaleChannel',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'DaysCoverChannel',
          field: 'daysCoverChannel',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'RateOfSale6Wk',
          field: 'rateOfSale6Wk',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'DaysCover6WkRos',
          field: 'daysCover6WkRos',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'RateOfSale21Day',
          field: 'rateOfSale21Day',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'DaysCover21DayRos',
          field: 'daysCover21DayRos',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'RateOfSale7Day',
          field: 'rateOfSale7Day',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'DaysCover7DayRos',
          field: 'daysCover7DayRos',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'StockOnHand',
          field: 'stockOnHand',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'ListingStatus',
          field: 'listingStatus',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'ListingStatusCode',
          field: 'listingStatusCode',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'ListingStatusDesc',
          field: 'listingStatusDesc',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'LastSold',
          field: 'lastSold',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'LastReceived',
          field: 'lastReceived',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'LastOrdered',
          field: 'lastOrdered',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'DaysSince_lastSold',
          field: 'daysSince_lastSold',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'DaysSince_lastOrdered',
          field: 'daysSince_lastOrdered',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'DaysSince_lastReceived',
          field: 'daysSince_lastReceived',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
        {
          headerName: 'Days_OrderToReceived',
          field: 'days_OrderToReceived',
          width: 150,
          sortingOrder: ["asc", "desc"],
          filter:true,
          sortable: true,
        },
    ];
    this.sortingOrder=["desc", "asc", null];

    this.SupplierID = this.getSupplierID();

    //fnc call
    this.service(this.SupplierID);

    //retrive object and store in supplier
    var retrievedObject = localStorage.getItem('supplierObject');
    this.supplier = JSON.parse(retrievedObject);


  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  //get the observable and store in localStorage
  service(id){
    this.subs.add(this.Auth.authStatus.subscribe(value => this.loggedIn = value));
    this.subs.add(this.shared.getSuppliers()
    .pipe(
      filter(emp => emp !== null),
      map(suppliers =>
        // console.log("suppliers", suppliers)
        suppliers.filter(supplier => supplier.SupplierID === id),
      )
    )
    .subscribe(response => {
      // console.log("From shared services", response);
      // console.log(this.SupplierID);
      // this.supplier = response;

      localStorage.setItem('supplierObject', JSON.stringify(response));
    }));
  }

   onGridReady(params){
    this.gridApi= params.api;
    this.gridColumnApi = params.columnApi;
    this.Authentication.reports()
    .toPromise().then(res => {
      //   console.log(res['SDC'])
        this.data = res['SDC'];
        // console.log(this.data)
        params.api.setRowData(this.data);
      })
    // .subscribe(data => {
    //   console.log(data);
    //   params.api.setRowData(data);
    // })
 }

 logout(event: MouseEvent) {
  event.preventDefault();
  this.Token.remove();
  localStorage.removeItem('supplierid');
  localStorage.removeItem('supplierObject');
  this.Auth.changeAuthStatus(false);
  this.router.navigateByUrl('/');
}

   onBtExport(){
     var params = {

     };
     // export to csv
     this.gridApi.exportDataAsCsv(params)
   }

   quickSearch() {
     this.gridApi.setQuickFilter(this.searchValue);
   }

   // load js file in components
   loadScript(url: string) {
    const body = <HTMLDivElement> document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = url;
    script.async = false;
    script.defer = true;
    body.appendChild(script);
  }

}
