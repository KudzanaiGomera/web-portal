import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectsupplierComponent } from './selectsupplier.component';

describe('SelectsupplierComponent', () => {
  let component: SelectsupplierComponent;
  let fixture: ComponentFixture<SelectsupplierComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectsupplierComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectsupplierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
