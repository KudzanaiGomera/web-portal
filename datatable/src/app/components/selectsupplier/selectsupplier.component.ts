import { Component, OnDestroy, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/Services/auth.service';
import { SelectsupplierService } from './../../Services/selectsupplier.service';
import { SharedService } from 'src/app/Services/shared.service';
import { TokenService } from 'src/app/Services/token.service';

import { Supplier } from './selectsupplierObj';
import { filter } from 'rxjs/operators';
import { SubSink } from 'subsink';


@Component({
  selector: 'app-selectsupplier',
  templateUrl: './selectsupplier.component.html',
  styleUrls: ['./selectsupplier.component.css']
})
export class SelectsupplierComponent implements OnInit, OnDestroy{
  public loggedIn: boolean;
  public suppliers: Supplier[] = [];
  data: any[] = [];

  public error = null;

  subs = new SubSink();

  constructor(
    private SelectSupplier: SelectsupplierService,
    private Auth: AuthService,
    private shared: SharedService,
    private router: Router,
    private Token: TokenService,
  ) { }

  ngOnInit(){
    //inject:js here
    // temporary bug fix due to js not loading on first page visit now load it from ngOnInit
    this.loadScript('./assets/js/off-canvas.js');
    this.loadScript('./assets/js/template.js');
    this.loadScript('./assets/js/hoverable-collapse.js');
    this.loadScript('./assets/js/settings.js');
    this.loadScript('./assets/js/todolist.js');
    this.subs.add(this.Auth.authStatus.subscribe(value => this.loggedIn = value));

    this.subs.add(this.SelectSupplier.selectsupplier()
    .pipe(filter(emp => emp !== null),)
    .subscribe(
      (data : Supplier[])=>{
        this.suppliers = data;
        this.shared.loadSuppliers(data);
      },
      (error) => console.log(error),
    ));
  }

  ngOnDestroy(){
    this.subs.unsubscribe();
  }

  logout(event: MouseEvent) {
    event.preventDefault();
    this.Token.remove();
    localStorage.removeItem('supplierid');
    this.Auth.changeAuthStatus(false);
    this.router.navigateByUrl('/');
  }

  handleError(error){
    this.error = error.error.Error;
  }

  handleResponse(data){
    this.Auth.changeAuthStatus(true);
  }

  // load js file in components
  loadScript(url: string) {
    const body = <HTMLDivElement> document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = url;
    script.async = false;
    script.defer = true;
    body.appendChild(script);
  }

}
