import { Component, OnDestroy, OnInit} from '@angular/core';
import { AuthService } from 'src/app/Services/auth.service';
import { AuthenticationService } from 'src/app/Services/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Supplier } from '../selectsupplier/selectsupplierObj';
import { SharedService } from 'src/app/Services/shared.service';
import { filter, map} from 'rxjs/operators';
import { TokenService } from 'src/app/Services/token.service';
import { SubSink } from 'subsink';

@Component({
  selector: 'app-site-days-cover',
  templateUrl: './site-days-cover.component.html',
  styleUrls: ['./site-days-cover.component.css']
})
export class SiteDaysCoverComponent implements OnInit, OnDestroy{
  public loggedIn: boolean;
  public searchValue;
  public gridApi;
  public gridColumnApi;
  public columnDefs;
  public sortingOrder;
  chosenReport: string = "";
  data: any[] = [];

  public SupplierID;
  public id;
  public supplier: Supplier[] = [];

  subs = new SubSink();


  constructor(
    private Authentication: AuthenticationService,
    private Auth: AuthService,
    private route: ActivatedRoute,
    private shared: SharedService,
    private router: Router,
    private Token: TokenService,
  ) {
    // this.route.params.subscribe( params => console.log(params) );
   }

   //Store Columns
   Store(){
    this.columnDefs = [
      {
        headerName: 'Province',
        field: 'Province',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'Banner',
        field: 'Banner',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'Profile',
        field: 'Profile',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'Size',
        field: 'Size',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'StoreName',
        field: 'StoreName',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'StoreID',
        field: 'StoreID',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'ArticleDescription',
        field: 'ArticleDescription',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'ArticleNumber',
        field: 'ArticleNumber',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'DROS_QTY',
        field: 'DROS_QTY',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'DaysCover',
        field: 'DaysCover',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'SOH',
        field: 'SOH',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'SiteArticleStatus',
        field: 'SiteArticleStatus',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'SiteArticleStatusDesc',
        field: 'SiteArticleStatusDesc',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'SourceOfSupply',
        field: 'SourceOfSupply',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'LastSold',
        field: 'LastSold',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'LastOrdered',
        field: 'LastOrdered',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'LastReceived',
        field: 'LastReceived',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'DaysSince_LastSold',
        field: 'DaysSince_LastSold',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'DaysSince_LastOrdered',
        field: 'DaysSince_LastOrdered',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'DaysOrderToReceived',
        field: 'DaysOrderToReceived',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },

  ];
  this.sortingOrder=["desc", "asc", null];
   }

   //DC Columns
   DC(){
    this.columnDefs = [
      {
        headerName: 'Province',
        field: 'Province',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'Banner',
        field: 'Banner',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'StoreName',
        field: 'StoreName',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'StoreID',
        field: 'StoreID',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'ArticleDescription',
        field: 'ArticleDescription',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'ArticleNumber',
        field: 'ArticleNumber',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'DROS_QTY',
        field: 'DROS_QTY',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'DaysCover',
        field: 'DaysCover',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'SOH',
        field: 'SOH',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'SiteArticleStatus',
        field: 'SiteArticleStatus',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'SiteArticleStatusDesc',
        field: 'SiteArticleStatusDesc',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'SourceOfSupply',
        field: 'SourceOfSupply',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'LastIssued',
        field: 'LastIssued',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'LastOrdered',
        field: 'LastOrdered',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'LastReceived',
        field: 'LastReceived',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'DaysSince_LastIssued',
        field: 'DaysSince_LastIssued',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'DaysSince_LastOrdered',
        field: 'DaysSince_LastOrdered',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },
      {
        headerName: 'DaysOrderToReceived',
        field: 'DaysOrderToReceived',
        width: 150,
        sortingOrder: ["asc", "desc"],
        filter:true,
        sortable: true,
      },

  ];
  this.sortingOrder=["desc", "asc", null];
   }


  //selecting the report to display
   report(){
    switch(this.chosenReport) {
      case "DC":
        this.onBtDCColumns();
        break;
      case "Store":
        this.onBtStoreColumns();
        break;
      default:
        console.log("Select Report");

    }
  }

  getSupplierID(){
    let supplierID = this.route.snapshot.paramMap.get('id');
    return (supplierID);
  }

   ngOnInit() {

    //inject:js here
    // temporary bug fix due to js not loading on first page visit now load it from ngOnInit
    this.loadScript('./assets/js/off-canvas.js');
    this.loadScript('./assets/js/template.js');
    this.loadScript('./assets/js/hoverable-collapse.js');
    this.loadScript('./assets/js/settings.js');
    this.loadScript('./assets/js/todolist.js');
    // call function on page load
    this.report();
    this.SupplierID = this.getSupplierID();

    //fnc call
    this.service(this.SupplierID);

    //retrive object and store in supplier
    var retrievedObject = localStorage.getItem('supplierObject');
    this.supplier = JSON.parse(retrievedObject);
  }

  ngOnDestroy(){
    this.subs.unsubscribe();
  }

  //get the observable and store in localStorage
  service(id){
    this.subs.add(this.Auth.authStatus.subscribe(value => this.loggedIn = value));
    this.subs.add( this.shared.getSuppliers()
    .pipe(
      filter(emp => emp !== null),
      map(suppliers =>
        // console.log("suppliers", suppliers)
        suppliers.filter(supplier => supplier.SupplierID === id),
      ),
    )
    .subscribe(response => {
      // console.log("From shared services", response);
      // console.log(this.SupplierID);
      // this.supplier = response;
      localStorage.setItem('supplierObject', JSON.stringify(response));
    }));
  }

  logout(event: MouseEvent) {
    event.preventDefault();
    this.Token.remove();
    localStorage.removeItem('supplierid');
    localStorage.removeItem('supplierObject');
    this.Auth.changeAuthStatus(false);
    this.router.navigateByUrl('/');
  }

  test(event: MouseEvent){
    event.preventDefault();
    alert("on click event works fine");
  }

  //adding columns for the Store table
  onBtStoreColumns() {
    this.gridApi.setColumnDefs(this.Store());
  }


  //removing some of Store Columns and adding DC
  onBtDCColumns() {
    this.gridApi.setColumnDefs(this.DC());
  }

  // pull data from api and populate the grid
  onGridReady(params){
    this.gridApi= params.api;
    this.gridColumnApi = params.columnApi;
    this.Authentication.sitedaycover()
    .toPromise().then(res => {
      this.data = res['STOCKOUT'];
      params.api.setRowData(this.data)
      })
 }

 //function to export reports
   onBtExport(){
     var params = {

     };
     // export to csv
     this.gridApi.exportDataAsCsv(params)
   }

   // function for a filtering all columns at once
   quickSearch(){
     this.gridApi.setQuickFilter(this.searchValue);
   }


  // load js file in components
   loadScript(url: string) {
    const body = <HTMLDivElement> document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = url;
    script.async = false;
    script.defer = true;
    body.appendChild(script);
  }

}
