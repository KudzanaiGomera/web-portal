import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/Services/auth.service';
import { AuthenticationService } from 'src/app/Services/authentication.service';
import { TokenService } from 'src/app/Services/token.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public form = {

    email:null,

    password:null
  };

  public error = null;

  constructor(
    private Authentication: AuthenticationService,
    private Token: TokenService,
    private router: Router,
    private Auth: AuthService,
    ) { }

  onSubmit(){
    this.Authentication.login(this.form).subscribe(
      data => this.handleResponse(data),
      error => this.handleError(error)
    );
  }

  handleError(error){
    this.error = error.error.Error;
  }

  handleResponse(data){
    this.Token.handle(data.token);
    this.Auth.changeAuthStatus(true);
    this.router.navigateByUrl('/selectsupplier');
  }

  ngOnInit(): void {
  }

}
