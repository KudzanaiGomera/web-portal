import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AppRoutingModule } from './app-routing.module';

import { AgGridModule } from 'ag-grid-angular';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './components/login/login.component';
import { AuthenticationService } from './Services/authentication.service';
import { TokenService } from './Services/token.service';
import { SiteDaysCoverComponent } from './components/site-days-cover/site-days-cover.component';
import { AuthService } from './Services/auth.service';
import { BeforeLoginService } from './Services/before-login.service';
import { AfterLoginService } from './Services/after-login.service';
import { ReportsComponent } from './components/reports/reports.component';
import { NumericDistributionComponent } from './components/numeric-distribution/numeric-distribution.component';
import { CustomTooltipComponent } from './components/custom-tooltip/custom-tooltip.component';
import { SelectsupplierComponent } from './components/selectsupplier/selectsupplier.component';
import { TokenInterceptorService } from './Services/token-interceptor.service';
import { PageNotFoundComponent } from './Components/page-not-found/page-not-found.component';
import { SharedService } from './Services/shared.service';
import { SelectsupplierService } from './Services/selectsupplier.service';
import { LocalstorageService } from './Services/localstorage.service';
import { GalleryComponent } from './components/gallery/gallery.component';
import { GalleryService } from './Services/gallery.service';
import { SharedgalleryService } from './Services/sharedgallery.service';
import { ScriptService } from './Services/script.service';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    SiteDaysCoverComponent,
    ReportsComponent,
    NumericDistributionComponent,
    CustomTooltipComponent,
    SelectsupplierComponent,
    PageNotFoundComponent,
    GalleryComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    HttpClientModule,
    FormsModule,
    AgGridModule.withComponents([CustomTooltipComponent])
  ],
  providers: [AuthenticationService, TokenService, AuthService, AfterLoginService, BeforeLoginService, SharedService, SelectsupplierService,LocalstorageService, GalleryService, SharedgalleryService, ScriptService,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptorService,
    multi: true
  }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
